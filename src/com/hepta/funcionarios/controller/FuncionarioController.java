package com.hepta.funcionarios.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.rest.FuncionarioService;
import com.hepta.funcionarios.rest.SetorService;

@ManagedBean(name = "funcionarioController")
@SessionScoped
public class FuncionarioController {

	public Funcionario funcionario = new Funcionario();
	public Setor setor = new Setor();
	public List<Setor> setores = new ArrayList<Setor>();
	public List<Funcionario> funcionarios = new ArrayList<Funcionario>();
	
	@PostConstruct
	public void init() {
		SetorService setorService = new SetorService();
		Response rsp = setorService.SetorRead();
		setores = (List<Setor>) rsp.getEntity();
		
	}
	
	public String cadastrarFuncionario() {
		FuncionarioService service = new FuncionarioService();
		
		Setor setor = new Setor();
		setor.setId(1);
		funcionario.setSetor(setor);
		
		service.FuncionarioCreate(funcionario);
		
		funcionario = new Funcionario();
		
		return voltar();
	}
	
	public void pesquisarFuncionario() {
		FuncionarioService service = new FuncionarioService();
		
		funcionarios = new ArrayList<>();
		Response read = service.FuncionarioRead();
		funcionarios = (List<Funcionario>) read.getEntity();
		
		
	}

	public String cadastrar() {
		return "/cadastrarFuncionario.xhtml";
	}
	public String voltar() {
		return "/index.html";
	}
	
	
	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public List<Setor> getSetores() {
		return setores;
	}

	public void setSetores(List<Setor> setores) {
		this.setores = setores;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	
	
}
