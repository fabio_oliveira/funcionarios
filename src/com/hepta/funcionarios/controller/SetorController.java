package com.hepta.funcionarios.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.rest.SetorService;

@ManagedBean(name = "SetorController")
@SessionScoped
public class SetorController {

	@Inject
	private SetorService service;

	public Setor setor = new Setor();

	private Response response;

	public void cadastrarSetor() {
		System.out.println(setor.getNome());

		setor.setNome("Setor");

		Setor setor = new Setor();
		setor.setId(1);

		service.SetorCreate(setor);
		System.out.println("deu certo");

		System.out.println(response.getStatus());

	}

	public Setor getSetor() {
		return setor;
	}

	public void setFuncionario(Setor setor) {
		this.setor = setor;
	}

}
